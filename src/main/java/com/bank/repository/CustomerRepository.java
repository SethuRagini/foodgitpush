package com.bank.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bank.entity.CustomerRegistration;

public interface CustomerRepository extends JpaRepository<CustomerRegistration,Long> {

	Optional<CustomerRegistration> findByFirstName(String firstName);
	
}
